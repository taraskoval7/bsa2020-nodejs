const responseMiddleware = (req, res, next) => {
	// TODO: Implement middleware that returns result of the query

	if (res.err)
		res.json({ 
			error: true, 
			message: res.err || 'Unknown error' 
		});
	
	res.status(200).json(res.data);
}

exports.responseMiddleware = responseMiddleware;