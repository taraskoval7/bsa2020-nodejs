const { user } = require('../models/user');
const UserService = require("../services/userService");

const createUserValid = (req, res, next) => {
	// TODO: Implement validatior for user entity during creation

	const { firstName, lastName, email, phoneNumber, password } = req.body;
	
	try {
		const user = UserService.getOne({ name });

		if (user) {
			res.status(400);
			console.log(`User with email '${email}' already exists`);
			res.err = `User with email '${email}' already exists`;

			return next();
		}
	} catch (err) {}


	// First name
	if (!firstName || firstName === "") {
		res.status(400);
		res.err = "First name cannot be an empty string";

		return next();
	}

	// Last name
	if (!lastName || lastName === "") {
		res.status(400);
		res.err = "Last name cannot be an empty string";

		return next();
	}

	// Phone number
	if (!phoneNumber || phoneNumber.indexOf("+380") !== 0) {
		res.status(400);
		res.err = "PhoneNumber number should be +380xxxxxxxxx";

		return next();
	}

	// Email
	if (!email || email.indexOf("@gmail.com") <= 0) {
		res.status(400);
		res.err = "Email address should be gmail";

		return next();
	}

	// Password
	if (!password || password.length < 3) {
		res.status(400);
		res.err = "Password should contain min 3 symbols";

		return next();
	}

	next();
}

const updateUserValid = (req, res, next) => {
	// TODO: Implement validatior for user entity during update

	const { id } = req.params;
	const { firstName, lastName, email, phoneNumber, password } = req.body;

	try {
		UserService.getOne({ id });

	} catch (err) {
		res.status(404);
		res.error = `User with id ${id} was not found`;

		return next();
	}


	// First name
	if (firstName && typeof firstName === "string" && firstName.length > 50) {
		res.status(400);
		res.err = "First name should not exceed 50 characters";

		return next();
	}

	// Last name
	if (lastName && typeof lastName === "string" && lastName.length > 50) {
		res.status(400);
		res.err = "Last name should not exceed 50 characters";

		return next();
	}

	// Phone number
	if (!phoneNumber || !(/\+380\d{9}/g).test(phoneNumber)) {
		res.status(400);
		res.err = "PhoneNumber number should be +380xxxxxxxxx";

		return next();
	}

	// Email
	if (!email || !(/^[a-z0-9](\.?[a-z0-9]){5,}@g(oogle)?mail\.com$/).test(email)) {
		res.status(400);
		res.err = "Email address should be gmail";

		return next();
	}

	// Password
	if (!password || password.length < 3) {
		res.status(400);
		res.err = "Password should contain min 3 symbols";

		return next();
	}

	next();
}

exports.createUserValid = createUserValid;
exports.updateUserValid = updateUserValid;