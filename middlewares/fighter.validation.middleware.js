const { fighter } = require('../models/fighter');

const createFighterValid = (req, res, next) => {
	// TODO: Implement validatior for fighter entity during creation

	const { name, power, defense } = req.body;

	try {
		const fighter = FighterService.getOne({ name });

		if (fighter) {
			res.status(400);
			res.err = `Fighter with name '${name}' already exists`;

			return next();
		}

	} catch (err) {}


	// Name
	if (!name || name === "") {
		res.status(400);
		res.err = "Name cannot be an empty string";

		return next();
	}

	// Power
	if (!power || isNaN(Number(power)) || Number(power) < 0 || Number(power) > 100) {
		res.status(400);
		res.err = "Power should be in range 0 - 100";

		return next();
	}

	// Defence
	if (!defense || isNaN(Number(defense)) || Number(defense) < 1 || Number(defense) > 10) {
		res.status(400);
		res.err = "Defence should be in range 1 - 10";

		return next();
	}

	next();
}

const updateFighterValid = (req, res, next) => {
	// TODO: Implement validatior for fighter entity during update

	const { id } = req.params;
	const { name, power, defense } = req.body;

	try {
		FighterService.getOne({ id });

	} catch (err) {
		res.status(404);
		res.error = `Fighter with id ${id} was not found`;

		return next();
	}


	// Name
	if (name && typeof name === "string" && name.length > 50) {
		res.status(400);
		res.err = "Name should not exceed 50 characters";

		return next();
	}

	// Defence
	if (defense && !isNaN(Number(defense)) && (Number(defense) < 1 || Number(defense) > 10)) {
		res.status(400);
		res.err = "Defence should be in range 1 - 10";
		
		return next();
	}

	next();
}

exports.createFighterValid = createFighterValid;
exports.updateFighterValid = updateFighterValid;