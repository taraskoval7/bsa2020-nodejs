const { Router } = require('express');
const UserService = require('../services/userService');
const { createUserValid, updateUserValid } = require('../middlewares/user.validation.middleware');
const { responseMiddleware } = require('../middlewares/response.middleware');

const router = Router();

// TODO: Implement route controllers for user

router.get("/", (req, res, next) => {
	try {
		res.data = UserService.getAll();

	} catch (err) {
		res.err = err;
		res.status(404);
	}

	next();
}, responseMiddleware);

router.get('/:id', updateUserValid, (req, res, next) => {
	if (res.err) 
		return next();

	const { id } = req.params;

	try {
		const { firstName, lastName, email, phone, password } = req.body;
		const dataToUpdate = {};

		if (firstName) dataToUpdate.firstName = firstName;
		if (lastName) dataToUpdate.lastName = lastName;
		if (email) dataToUpdate.email = email;
		if (phone) dataToUpdate.phone = phone;
		if (password) dataToUpdate.password = password;

		res.data = UserService.update(id, dataToUpdate);

	} catch (err) {
		res.err = err;
		res.status(400);
	}

	next();
});

router.post('/', createUserValid, (req, res, next) => {
	if (res.err) 
		return next();

	const user = UserService.create(req.body);

	res.data = user;

	try {
		res.data = UserService.create({ firstName, lastName, email, phone, password });

	} catch (err) {
		res.err = err;
		res.status(400);
	}

	next();

}, responseMiddleware);

module.exports = router;