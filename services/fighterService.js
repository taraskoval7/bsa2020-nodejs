const { FighterRepository } = require('../repositories/fighterRepository');

class FighterService {
	// TODO: Implement methods to work with fighters

	create(fighter) {
		/*const item = UserRepository.create(fighter);

		if (!item)
			throw Error('Fighter was not created');
		return item;*/

		if (this.search({name: fighter.name}))
			return null;

		return FighterRepository.create(fighter);
	}

	update(id, fighter) {
		/*const item = UserRepository.update(id, fighter);

		if (!item)
			throw Error('Fighter was not updated');
		return item;*/

		const item = this.search({id});
		if (!item)
			return null;

		return FighterRepository.update(id, fighter);
	}

	getAll() {
		const items = UserRepository.getAll();
		
		if (!items) {
			// return [];
			return null;
		}
		return items;
	}

	delete(id) {
		const item = this.search({id})
		if (!item)
			return null;

		return FighterRepository.delete(id);
	}

	getOne(search) {
		const item = UserRepository.getOne(search);

		if (!item)
			throw Error('Fighter not found');
		return item;
	}
}

module.exports = new FighterService();