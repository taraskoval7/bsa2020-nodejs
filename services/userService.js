const { UserRepository } = require('../repositories/userRepository');

class UserService {

	// TODO: Implement methods to work with user

	create(user) {
		/*const item = UserRepository.create(user);

		if (!item)
			return null;
		return item;*/

		if (this.search({email: user.email}) || 
			this.search({phoneNumber: user.phoneNumber}))
			return null;

		return UserRepository.create(user);
	}

	update(id, user) {
		/*const item = UserRepository.update(id, user);

		if (!item)
			throw Error('User was not updated');
		return item;*/

		const item = this.search({id})
		if (!item)
			return null;

		return UserRepository.update(id, user);
	}

	delete(id) {
		const item = this.search({id})

		if (!item)
			return null;

		return UserRepository.delete(id);
	}

	getAll() {
		const items = UserRepository.getAll();
		
		if (!items) {
			// return [];
			return null;
		}
		return items;
	}

	search(search) {
		const item = UserRepository.getOne(search);

		if (!item)
			return null;
		return item;
	}
}

module.exports = new UserService();